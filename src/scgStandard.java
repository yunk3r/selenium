import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class scgStandard {

	static List<WebElement> list;
	static ArrayList<WebElement> array=new ArrayList<WebElement>();
	static org.openqa.selenium.interactions.Actions action;
	static SqlAccess scg=new SqlAccess();
	static String deckName;
	static String finish;
	static String player;
	static String event;
	static String date;
	static String location;
	static String deck;
	static String stopDate1;
	static String stopDate2;
	


	public static void main(String[] args) throws Exception {

		
		//scg.TruncateTable("scg");
		stopDate1=scg.ReadDatabase("scg")[0];
		stopDate2=scg.ReadDatabase("scg")[1];
		WebDriver driver= new FirefoxDriver();
		driver.get("http://sales.starcitygames.com//deckdatabase/deckshow.php?t[T1]=1&event_ID=19&feedin=&start_date=9%2F26%2F2015&end_date=12%2F30%2F2016&city=&state=&country=&start=&finish=&exp=&p_first=&p_last=&simple_card_name[1]=&simple_card_name[2]=&simple_card_name[3]=&simple_card_name[4]=&simple_card_name[5]=&w_perc=0&g_perc=0&r_perc=0&b_perc=0&u_perc=0&a_perc=0&comparison[1]=%3E%3D&card_qty[1]=1&card_name[1]=&comparison[2]=%3E%3D&card_qty[2]=1&card_name[2]=&comparison[3]=%3E%3D&card_qty[3]=1&card_name[3]=&comparison[4]=%3E%3D&card_qty[4]=1&card_name[4]=&comparison[5]=%3E%3D&card_qty[5]=1&card_name[5]=&sb_comparison[1]=%3E%3D&sb_card_qty[1]=1&sb_card_name[1]=&sb_comparison[2]=%3E%3D&sb_card_qty[2]=1&sb_card_name[2]=&card_not[1]=&card_not[2]=&card_not[3]=&card_not[4]=&card_not[5]=&order_1=date+desc&order_2=finish&limit=25&action=Show+Decks&p=1");

		action=new org.openqa.selenium.interactions.Actions(driver);
		WebElement table;
		table=driver.findElement(By.xpath("/html/body/section/div/section/table/tbody"));
		list=table.findElements(By.tagName("tr"));
		for(WebElement row : list)
		{
			array.add(row);	
		}	 

		try{
			do
			{

				for(int i=5;i<array.size()-1;i++)
				{	 
					Thread.sleep(1000);
					array.clear();
					table=driver.findElement(By.xpath("/html/body/section/div/section/table/tbody"));
					list=table.findElements(By.tagName("tr"));
					for(WebElement row : list)
					{
						array.add(row);	
					}	 
					Thread.sleep(800);

					deckName=array.get(i).findElements(By.xpath("td")).get(0).getText();
				
					finish=array.get(i).findElements(By.xpath("td")).get(1).getText();

					player=array.get(i).findElements(By.xpath("td")).get(2).getText();

					event=array.get(i).findElements(By.xpath("td")).get(3).getText();

					date=array.get(i).findElements(By.xpath("td")).get(4).getText();

					location=array.get(i).findElements(By.xpath("td")).get(5).getText();

					Thread.sleep(800);
					WebElement item= array.get(i);
					Thread.sleep(1000);
					item.findElement(By.partialLinkText(item.getText().substring(0,4))).click();
					Thread.sleep(1000);
					deck=driver.findElement(By.xpath("/html/body/section/div/section/div/div/div[3]")).getText();

					Thread.sleep(800);
					
					driver.navigate().back();
					Thread.sleep(1000);
					
					if(date.equals(stopDate1)||date.equals(stopDate2))
					{
						driver.close();
						return;
					}
					scg.EnterDataScgStandard(deckName, finish, player, event, date, location, deck);

				}
				driver.findElement(By.partialLinkText("- Next>>")).click();
				Thread.sleep(800);
			}while(driver.findElement(By.partialLinkText("Next>>")).isDisplayed());
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			System.out.println(e.getMessage());
		}


		for(int i=5;i<array.size()-1;i++)
		{	 
			Thread.sleep(1000);
			array.clear();
			table=driver.findElement(By.xpath("/html/body/section/div/section/table/tbody"));
			list=table.findElements(By.tagName("tr"));
			for(WebElement row : list)
			{
				array.add(row);	
			}	 
			Thread.sleep(800);

			deckName=array.get(i).findElements(By.xpath("td")).get(0).getText();

			finish=array.get(i).findElements(By.xpath("td")).get(1).getText();

			player=array.get(i).findElements(By.xpath("td")).get(2).getText();

			event=array.get(i).findElements(By.xpath("td")).get(3).getText();

			date=array.get(i).findElements(By.xpath("td")).get(4).getText();

			location=array.get(i).findElements(By.xpath("td")).get(5).getText();



			Thread.sleep(800);
			WebElement item= array.get(i);
			Thread.sleep(1000);
			item.findElement(By.partialLinkText(item.getText().substring(0,4))).click();
			Thread.sleep(1000);
			deck=driver.findElement(By.xpath("/html/body/section/div/section/div/div/div[3]")).getText();


			Thread.sleep(800);
			driver.navigate().back();
			Thread.sleep(1000);
			
			if(date.equals(stopDate1)||date.equals(stopDate2))
			{
				driver.close();
				return;
			}
			scg.EnterDataScgStandard(deckName, finish, player, event, date, location, deck);


		}

			driver.close();
		}

	}
