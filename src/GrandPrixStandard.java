import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GrandPrixStandard 
{

	static ArrayList<WebElement> array=new ArrayList<WebElement>();
	static List<WebElement> list;
	static String name;
	static String player;
	static String event;
	static String rank;
	static String date;
	static String cards;
	static SqlAccess gp=new SqlAccess();
	static String stopDate2;
	static String stopDate1;

	public static void main(String[] args) throws Exception
	{


		WebDriver driver= new FirefoxDriver();
		//gp.TruncateTable("gpStandard"); 
		driver.get("http://www.mtgtop8.com/search");
		stopDate1=gp.ReadDatabase("gpStandard")[0];
		stopDate2=gp.ReadDatabase("gpStandard")[1];
			
		driver.findElement(By.name("compet_check[M]")).click();
		driver.findElement(By.name("compet_check[C]")).click();
		driver.findElement(By.name("compet_check[R]")).click();
		Thread.sleep(1000);
		driver.findElement(By.name("format")).sendKeys("s");
		driver.findElement(By.name("format")).sendKeys(Keys.ENTER);
		Thread.sleep(2500);
		driver.findElement(By.name("search_form")).findElements(By.tagName("tr")).get(12).click();
		do
		{
			WebElement table;
			Thread.sleep(1500);
			try
			{
				table=driver.findElement(By.name("compare_decks"));
			}
			catch(Exception e)
			{
				try
				{
				System.out.println("B1");
				Thread.sleep(2000);
				driver.navigate().back();
				Thread.sleep(2000);
				driver.navigate().forward();
				table=driver.findElement(By.name("compare_decks"));
				}
				catch(Exception c)
				{
					System.out.println("B2");
					Thread.sleep(2000);
					driver.navigate().back();
					Thread.sleep(2000);
					driver.navigate().forward();
					table=driver.findElement(By.name("compare_decks"));
				}
			}

			list=table.findElements(By.tagName("tr"));

			for(WebElement row: list)
			{
				array.add(row);
			}

			for(int i=4;i<array.size()-1;i++)
			{
				Thread.sleep(3000);
				array.clear();
				try
				{
					table=driver.findElement(By.name("compare_decks"));
				}
				catch(Exception e)
				{
					try
					{
					System.out.println("F1");
					driver.navigate().forward();
					Thread.sleep(4000);
					driver.navigate().back();
					Thread.sleep(4000);
					table=driver.findElement(By.name("compare_decks"));
					}
					catch(Exception x)
					{
						try
						{
						System.out.println("F2");
						driver.navigate().forward();
						Thread.sleep(5000);
						driver.navigate().back();
						Thread.sleep(5000);
						table=driver.findElement(By.name("compare_decks"));
						}
						catch(Exception c)
						{
							System.out.println("F3");
							driver.navigate().forward();
							Thread.sleep(5000);
							driver.navigate().back();
							Thread.sleep(5000);
							driver.navigate().back();
							Thread.sleep(2000);
							table=driver.findElement(By.name("compare_decks"));
						}
					}
					}

				list=table.findElements(By.tagName("tr"));

				for(WebElement row: list)
				{
					array.add(row);
				}
				name=array.get(i).findElements(By.tagName("td")).get(1).getText();
				System.out.println(name);
				player=array.get(i).findElements(By.tagName("td")).get(2).getText();
				event=array.get(i).findElements(By.tagName("td")).get(3).getText();
				rank=array.get(i).findElements(By.tagName("td")).get(5).getText();
				date=array.get(i).findElements(By.tagName("td")).get(6).getText();

				if(date.equals("30/08/15"))
				{
					break;
				}
				
				

				array.get(i).findElement(By.linkText(name)).click();
				Thread.sleep(2000);
				try
				{
					cards=driver.findElement(By.xpath("/html/body/div[3]/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr")).getText();
					
				}									
				catch(Exception e)
				{
					try
					{
						driver.navigate().refresh();
						System.out.println("1");
						Thread.sleep(6000);
						cards=driver.findElement(By.xpath("/html/body/div[3]/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr")).getText();

					}

					catch(Exception x)
					{
						try
						{
							driver.navigate().refresh();
							System.out.println("2");
							Thread.sleep(8000);
							cards=driver.findElement(By.xpath("/html/body/div[3]/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr")).getText();
						}
						catch(Exception c)
						{
							try
							{
							driver.navigate().refresh();
							System.out.println("3");
							Thread.sleep(8000);
							cards=driver.findElement(By.xpath("/html/body/div[3]/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr")).getText();
							}
							catch(Exception a)
							{
								driver.navigate().refresh();
								System.out.println("4");
								Thread.sleep(4000);
								cards=driver.findElement(By.xpath("/html/body/div[3]/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr")).getText();
							}
						}
					}

				}
				
				if(date.equals(stopDate1)||date.equals(stopDate2))
				{
					driver.close();
					return;
				}
				gp.EnterDataGpStandard(name,rank, player, event, date,cards);
				Thread.sleep(1000);
				driver.navigate().back();
				Thread.sleep(1000);

			}

			if(driver.findElements(By.className("Nav_PN")).size()==2)
			{
				driver.findElements(By.className("Nav_PN")).get(1).click();
			}
			else
			{
				driver.findElement(By.className("Nav_PN")).click();
			}
		}while(!date.equals("30/08/15"));




	}

}
