
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class scgModernClassic{

	static List<WebElement> list;
	static ArrayList<WebElement> array=new ArrayList<WebElement>();
	static org.openqa.selenium.interactions.Actions action;
	static SqlAccess scg=new SqlAccess();
	static String deckName;
	static String finish;
	static String player;
	static String event;
	static String date;
	static String location;
	static String deck;
	static String stopDate1;
	static String stopDate2;
	


	public static void main(String[] args) throws Exception {

		
		//scg.TruncateTable("scgModernClassic");
		stopDate1=scg.ReadDatabase("scgModernClassic")[0];
		stopDate2=scg.ReadDatabase("scgModernClassic")[1];
		WebDriver driver= new FirefoxDriver();
		driver.get("http://sales.starcitygames.com//deckdatabase/deckshow.php?&t[C1]=28&start_date=10/08/2015&end_date=01/31/2017&event_ID=36&order_1=date%20desc&order_2=finish&start_num=25&start_num=0&limit=25");

		action=new org.openqa.selenium.interactions.Actions(driver);
		WebElement table;
		table=driver.findElement(By.xpath("/html/body/section/div/section/table/tbody"));
		list=table.findElements(By.tagName("tr"));
		for(WebElement row : list)
		{
			array.add(row);	
		}	 

		try{
			do
			{

				for(int i=5;i<array.size()-1;i++)
				{	 
					Thread.sleep(1000);
					array.clear();
					table=driver.findElement(By.xpath("/html/body/section/div/section/table/tbody"));
					list=table.findElements(By.tagName("tr"));
					for(WebElement row : list)
					{
						array.add(row);	
					}	 
					Thread.sleep(800);

					deckName=array.get(i).findElements(By.xpath("td")).get(0).getText();
				
					finish=array.get(i).findElements(By.xpath("td")).get(1).getText();

					player=array.get(i).findElements(By.xpath("td")).get(2).getText();

					event=array.get(i).findElements(By.xpath("td")).get(3).getText();

					date=array.get(i).findElements(By.xpath("td")).get(4).getText();

					location=array.get(i).findElements(By.xpath("td")).get(5).getText();

					Thread.sleep(800);
					WebElement item= array.get(i);
					Thread.sleep(1000);
					item.findElement(By.partialLinkText(item.getText().substring(0,2))).click();
					Thread.sleep(1000);
					deck=driver.findElement(By.xpath("/html/body/section/div/section/div/div/div[3]")).getText();

					Thread.sleep(800);
					
					driver.navigate().back();
					Thread.sleep(1000);
					
					if(date.equals(stopDate1)||date.equals(stopDate2))
					{
						driver.close();
						return;
					}
					scg.EnterDataScgModernClassic(deckName, finish, player, event, date, location, deck);

				}
				driver.findElement(By.partialLinkText("- Next>>")).click();
				Thread.sleep(800);
			}while(driver.findElement(By.partialLinkText("Next>>")).isDisplayed());
		}
		catch(org.openqa.selenium.NoSuchElementException e)
		{
			System.out.println(e.getMessage());
		}


		for(int i=5;i<array.size()-1;i++)
		{	 
			Thread.sleep(1000);
			array.clear();
			table=driver.findElement(By.xpath("/html/body/section/div/section/table/tbody"));
			list=table.findElements(By.tagName("tr"));
			for(WebElement row : list)
			{
				array.add(row);	
			}	 
			Thread.sleep(800);

			deckName=array.get(i).findElements(By.xpath("td")).get(0).getText();

			finish=array.get(i).findElements(By.xpath("td")).get(1).getText();

			player=array.get(i).findElements(By.xpath("td")).get(2).getText();

			event=array.get(i).findElements(By.xpath("td")).get(3).getText();

			date=array.get(i).findElements(By.xpath("td")).get(4).getText();

			location=array.get(i).findElements(By.xpath("td")).get(5).getText();



			Thread.sleep(800);
			WebElement item= array.get(i);
			Thread.sleep(1000);
			item.findElement(By.partialLinkText(item.getText().substring(0,2))).click();
			Thread.sleep(1000);
			deck=driver.findElement(By.xpath("/html/body/section/div/section/div/div/div[3]")).getText();


			Thread.sleep(1000);
			driver.navigate().back();
			Thread.sleep(1000);
			
			if(date.equals(stopDate1)||date.equals(stopDate2))
			{
				driver.close();
				return;
			}
			scg.EnterDataScgModernClassic(deckName, finish, player, event, date, location, deck);


		}

			driver.close();
		}

	}
